# TODO

* ViewsFormBase::getForm redirects to URL from views_ui_build_form_url()
    * => Subclass all forms: AddHandler, EditDetails, ReorderDisplays, Rearrange, RearrangeFilter, Display, ConfigHandler, ConfigHandlerExtra, ConfigHandlerGroup  

## Later

* ViewAddForm,ViewDuplicateForm,BreakLockForm,Analyze
  ::submitForm redirects to URL from ViewUI::urlInfo
    *  => Decorate ViewUI.
